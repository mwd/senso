import * as React from 'react';
import { useEffect, useState } from 'react';
import useCockpitContext from '../hooks/useCockpitContext';
import useStoreContext from '../hooks/useStoreContext';
import {
    defaultSensoContext,
    DeviceState,
    Goal,
    SensoContext,
    Track,
} from '../types';
import { calculateScore } from '../utils';

export const context = React.createContext<SensoContext>({
    ...defaultSensoContext,
});

interface SensoProviderProps {
    children?: React.ReactNode;
}

export default function SensoProvider({ children }: SensoProviderProps) {
    const {
        scores,
        updateStorage,
        difficulty,
        currentTrackId,
    } = useStoreContext();
    const { tracks } = useCockpitContext();

    const [currentTrack, setCurrentTrack] = useState<Track>(null);
    const [gameState, setGameState] = useState<DeviceState>('idle');

    const [goal, setGoal] = useState<Goal | null>(null);

    useEffect(() => {
        if (!currentTrackId && tracks.length) {
            handleCurrentTrack(tracks[0].id);
        } else {
            handleCurrentTrack(currentTrackId);
        }
    }, [tracks, currentTrackId]);

    const handleCurrentTrack = (trackId: Track['id']) => {
        setCurrentTrack(tracks.find((track) => track.id === trackId));
        if (!currentTrackId) {
            // ?
        }
        updateStorage({ currentTrackId: trackId });
    };

    const handleGoal = (goal: Goal) => {
        switch (goal.type) {
            default:
                // ... ?
                return;
            case 'win':
                let message;
                if (
                    !scores[currentTrack.id] ||
                    calculateScore(scores[currentTrack.id]) <
                        calculateScore(goal.score)
                ) {
                    message = 'nice! you did improve';
                    updateStorage({
                        scores: {
                            ...scores,
                            [currentTrack.id]: goal.score,
                        },
                    });
                }
                setGoal({
                    ...goal,
                    message,
                });
                break;
            case 'fail':
                setGoal(goal);
                break;
        }
    };

    return (
        <context.Provider
            value={{
                currentTrack,
                // todo check if memoized function would be appropriate
                selectTrack: handleCurrentTrack,
                deviceState: gameState,
                setDeviceState: setGameState,
                goal,
                handleGoal,
                difficulty,
                setDifficulty: (difficulty) => updateStorage({ difficulty }),
            }}
        >
            {children}
        </context.Provider>
    );
}
