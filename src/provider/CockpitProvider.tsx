import * as React from 'react';
import { useEffect, useState } from 'react';
import useStoreContext from '../hooks/useStoreContext';
import { sound } from '../soundMap';
import {
    CockpitContext,
    Collections,
    defaultCockpitContext,
    HighScore,
    Score,
    Track,
} from '../types';
import { calculateScore, padDigits } from '../utils';

const transformTracks = (collection) =>
    collection.entries.map(({ _id: id, name, steps, rating }) => ({
        id,
        name,
        rating,
        steps: steps
            .split(/\n/)
            .map((item) => item.trim())
            .map((step) => {
                const [soundKey, duration] = step
                    .split(',')
                    .map((item) => item.trim());
                return {
                    sound: sound[soundKey],
                    duration: parseInt(duration, 10),
                };
            }),
    }));

const transformHighScores = (collection) => {
    return collection.entries.map((item) => {
        return {
            ...item,
            date: item._created,
        };
    });
};

export const context = React.createContext<CockpitContext>({
    ...defaultCockpitContext,
});

const apiToken = 'b915759048381c082c495a094c4d4a';
const cockpitApiPath = `https://gewinnermusik.de/cp/api/collections/`;

interface CockpitProviderProps {
    children?: React.ReactNode;
}

export default function CockpitProvider({ children }: CockpitProviderProps) {
    const [error, setError] = useState(false);
    const [tracks, setTracks] = useState<Track[]>([]);
    const [highScores, setHighScores] = useState<HighScore[]>([]);
    const [sync, setSync] = useState(0);

    const {
        user,
        updateStorage,
        scores,
        currentTrackId,
        synced,
    } = useStoreContext();

    // filter: {published:true},
    // fields: {fieldA: 1, fieldB: 1},
    // limit: 10,
    // skip: 5,
    // sort: {
    //     rating: 1,
    //         name: 1,
    // },

    async function getData<M = any>(
        what: Collections,
        body: {},
        transform: (result) => M[]
    ) {
        const res = await fetch(
            `${cockpitApiPath}get/${what}?token=${apiToken}`,
            {
                method: 'post',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(body),
            }
        );

        return res
            .json()
            .then((res) => transform(res))
            .catch((err) => setError(err));
    }

    async function saveData<M = any>(what: Collections, body: {}) {
        const res = await fetch(
            `${cockpitApiPath}save/${what}?token=${apiToken}`,
            {
                method: 'post',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(body),
            }
        );
        return res.json().catch((err) => setError(err));
    }

    useEffect(() => {
        getData<Track>(
            'sensoTracks',
            {
                sort: {
                    rating: 1,
                    name: 1,
                },
            },
            transformTracks
        ).then((res) => res && setTracks(res));
    }, [setTracks]);

    useEffect(() => {
        if (!currentTrackId) return;
        getData<HighScore>(
            'sensoHighScores',
            {
                filter: { trackId: currentTrackId },
                limit: 3,
                sort: {
                    score: -1,
                },
            },
            transformHighScores
        ).then((res) => {
            res && setHighScores(res);
        });
    }, [currentTrackId, setHighScores, sync]);

    const uploadScore = (score: Score, trackId: Track['id']) => {
        saveData('sensoHighScores', {
            data: {
                score: padDigits(calculateScore(score), 8),
                user,
                trackId,
            },
        }).then(() => {
            updateStorage({
                scores: {
                    ...scores,
                    [trackId]: {
                        ...score,
                        synced: true,
                    },
                },
            });
            setSync(sync + 1);
        });
    };

    return (
        <context.Provider
            value={{ tracks, highScores, uploadScore, synced, error }}
        >
            {children}
        </context.Provider>
    );
}
