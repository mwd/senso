import * as React from 'react';
import { useCallback, useState } from 'react';
import { storageKey } from '../app';
import {
    defaultStore,
    defaultStoreContext,
    SensoStore,
    SensoStoreContext,
} from '../types';

export const context = React.createContext<SensoStoreContext>({
    ...defaultStoreContext,
});

interface StoreProviderProps {
    children?: React.ReactNode;
}

export default function StoreProvider({ children }: StoreProviderProps) {
    const [synced, setSynced] = useState(new Date().getTime());

    // init
    if (!localStorage.getItem(storageKey)) {
        localStorage.setItem(storageKey, JSON.stringify(defaultStore));
    }

    // setup
    const [storage, setStorage] = useState<SensoStore>({
        ...defaultStore, // sanitizes store only if properties are added
        ...JSON.parse(localStorage.getItem(storageKey)),
    });

    // persist changes
    const updateStorage = useCallback(
        (update: Partial<Storage>) => {
            setStorage((current) => {
                const nextStorage = { ...current, ...update };
                localStorage.setItem(
                    storageKey,
                    JSON.stringify({ ...nextStorage })
                );
                setSynced(new Date().getTime());
                return nextStorage;
            });
        },
        [setStorage, setSynced]
    );

    return (
        <context.Provider value={{ ...storage, updateStorage, synced }}>
            {children}
        </context.Provider>
    );
}
