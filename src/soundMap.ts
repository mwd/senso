import { Sound } from './types';

export const sounds = {
    red: 'red.mp3',
    green: 'green.mp3',
    blue: 'blue.mp3',
    yellow: 'yellow.mp3',
    fail: 'fail.mp3',
    win: 'win.mp3',
};

export const sound: Sound[] = ['green', 'red', 'blue', 'yellow'];

export default (sound: Sound) => `./assets/audio/${sounds[sound]}`;
