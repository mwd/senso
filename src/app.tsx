import * as React from 'react';
import * as ReactDOM from 'react-dom';
import 'typeface-spartan';
import Senso from './components/Senso';
import User from './components/User';
import CockpitProvider from './provider/CockpitProvider';
import SensoProvider from './provider/SensoProvider';
import StoreProvider from './provider/StoreProvider';
import './app.scss';

export const storageKey = 'mwd-senso';
export const isTouchDevice = 'ontouchstart' in window;

function App() {
    return (
        <React.Suspense fallback={<div>loading app</div>}>
            <StoreProvider>
                <CockpitProvider>
                    <User />
                    <SensoProvider>
                        <Senso />
                    </SensoProvider>
                </CockpitProvider>
            </StoreProvider>
        </React.Suspense>
    );
}

ReactDOM.render(<App />, document.getElementById('app'));
