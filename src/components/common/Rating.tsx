import * as React from 'react';
import StarIcon from '../icons/StarIcon';

interface RatingProps {
    rating: number;
    ratingValue: number;

    [index: string]: any;
}

export default function Rating({
    rating = 5,
    ratingValue = 0,
    ...props
}: RatingProps) {
    const renderStars = () => {
        const stars = [];
        for (let i = 0; i < rating; i++) {
            stars.push(
                <StarIcon
                    key={i}
                    className={i < ratingValue ? 'isOn' : 'isOff'}
                />
            );
        }
        return stars;
    };

    return <div {...props}>{renderStars()}</div>;
}
