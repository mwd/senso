import * as React from 'react';
import './VerticalLoader.scss';

export default function VerticalLoader() {
    return <div className={'verticalLoader'} />;
}
