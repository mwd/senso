import clsx from 'clsx';
import * as React from 'react';
import './Modal.scss';

interface ModalProps {
    children?: React.ReactNode;
    [index: string]: any;
}

export default function Modal({ children, className, ...props }: ModalProps) {
    return (
        <div className={clsx('modal', className)} {...props}>
            {children}
        </div>
    );
}
