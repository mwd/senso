import * as React from 'react';
import { useEffect, useState } from 'react';

interface ExpireProps {
    children: React.ReactElement;
    duration?: number;
    dissolveDuration?: number;
    onAway?: () => void;
}

export default function Expire({
    children: Component,
    duration = 3000,
    dissolveDuration = 0,
    onAway,
}: ExpireProps) {
    const [status, setStatus] = useState<'firm' | 'dissolve' | 'away'>('firm');

    useEffect(() => {
        let timer;

        if (status === 'away') {
            return undefined;
        }
        if (status === 'firm') {
            timer = setTimeout(() => {
                setStatus(dissolveDuration ? 'dissolve' : 'away');
            }, duration - dissolveDuration);
        }
        if (status === 'dissolve') {
            clearTimeout(timer);
            timer = setTimeout(() => {
                setStatus('away');
                if (onAway) {
                    onAway();
                }
            }, dissolveDuration);
        }

        return () => clearTimeout(timer);
    }, [status, setStatus]);

    return (
        <>
            {status === 'away'
                ? null
                : React.cloneElement(Component, {
                      ...Component.props,
                      className: `${Component.props.className} ${status}`,
                  })}
        </>
    );
}
