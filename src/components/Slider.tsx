import * as React from 'react';
import { useRef } from 'react';
import Draggable from 'react-draggable';
import useSensoContext from '../hooks/useSensoContext';
import { Difficulty } from '../types';
import './Slider.scss';

export default function Slider() {
    const { difficulty, setDifficulty } = useSensoContext();
    const offsetParent = useRef<HTMLDivElement>(null);

    return (
        <div
            className={'slider'}
            onClick={() => {
                setDifficulty(
                    (difficulty < 3 ? difficulty + 1 : 1) as Difficulty
                );
            }}
        >
            <div ref={offsetParent} className="slider__track">
                <Draggable
                    position={{ x: Math.floor((difficulty - 1) * 50), y: 0 }}
                    axis={'x'}
                    grid={[50, null]}
                    bounds={{ left: 0, right: 100 }}
                    onStop={(e, data) => {
                        setDifficulty((data.lastX / 50 + 1) as Difficulty);
                    }}
                >
                    <div className="slider__handle" />
                </Draggable>
            </div>
            <div className="slider__label">
                <span>easy</span>
                <span>medium</span>
                <span>hard</span>
            </div>
        </div>
    );
}
