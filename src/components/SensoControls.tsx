import * as React from 'react';
import useCockpitContext from '../hooks/useCockpitContext';
import useSensoContext from '../hooks/useSensoContext';
import LabeledButton from './LabeledButton';
import './SensoControls.scss';
import SensoTrackItem from './SensoTrackItem';
import Slider from './Slider';

export default function SensoControls() {
    const { currentTrack, deviceState, selectTrack } = useSensoContext();
    const { tracks } = useCockpitContext();

    const trackIds = tracks.map((track) => track.id);

    const toggleTrack = (what: 'prev' | 'next') => {
        const index = trackIds.findIndex((i) => i === currentTrack.id);

        if (what === 'prev') {
            selectTrack(trackIds[index > 0 ? index - 1 : tracks.length - 1]);
        }
        if (what === 'next') {
            selectTrack(trackIds[index < tracks.length - 1 ? index + 1 : 0]);
        }
    };

    return (
        <>
            <div className="sensoControls__controls">
                <LabeledButton
                    label="prev"
                    onClick={() => toggleTrack('prev')}
                />
                <Slider />
                <LabeledButton
                    label="next"
                    onClick={() => toggleTrack('next')}
                />
            </div>
            <div className="sensoControls__info">
                {deviceState === 'preparing' ? (
                    'waiting for your input'
                ) : (
                    <SensoTrackItem track={currentTrack} />
                )}
            </div>
        </>
    );
}
