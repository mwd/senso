import * as React from 'react';
import { useState } from 'react';
import useCockpitContext from '../hooks/useCockpitContext';
import useSensoContext from '../hooks/useSensoContext';
import Expire from './common/Expire';
import Modal from './common/Modal';
import VerticalLoader from './common/VerticalLoader';
import MwdLogo from './icons/MwdLogo';
import './Senso.scss';
import SensoControls from './SensoControls';
import SensoDevice from './SensoDevice';
import SensoGoal from './SensoGoal';
import SensoHighScores from './SensoHighScores';
import SensoTracks from './SensoTracks';

export default function Senso() {
    const { currentTrack, goal, handleGoal, deviceState } = useSensoContext();
    const { tracks } = useCockpitContext();

    const [showGoal, setShowGoal] = useState(false);

    return (
        <>
            {showGoal && goal && deviceState === 'idle' && (
                <Expire
                    duration={goal.type === 'win' ? 8000 : 4000}
                    dissolveDuration={1000}
                    onAway={() => setShowGoal(false)}
                >
                    <Modal
                        className={'sensoGoal__modal'}
                        onClick={() => setShowGoal(false)}
                    >
                        <SensoGoal />
                    </Modal>
                </Expire>
            )}
            {tracks && currentTrack ? (
                <div className="senso">
                    <SensoDevice
                        onGoal={(goal) => {
                            handleGoal(goal);
                            setShowGoal(true);
                        }}
                    />
                    <div className="senso__game">
                        <SensoControls />
                        <div className="senso__aside">
                            <SensoHighScores />
                            <SensoTracks />
                        </div>
                    </div>
                    <div className="senso__footer">
                        <div className="senso__footer__copy">a mwd project</div>
                        <MwdLogo className="senso__footer__logo" />
                    </div>
                </div>
            ) : (
                <VerticalLoader />
            )}
        </>
    );
}
