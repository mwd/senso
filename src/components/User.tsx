import clsx from 'clsx';
import * as React from 'react';
import { useEffect, useRef, useState } from 'react';
import useStoreContext from '../hooks/useStoreContext';
import FaceIcon from './icons/FaceIcon';
import './User.scss';

export default function User() {
    const [edit, setEdit] = useState(false);
    const { user, updateStorage } = useStoreContext();
    const [name, setName] = useState(user || '');

    useEffect(() => {
        if (user === name) return;
        let timer;
        timer = setTimeout(() => {
            updateStorage({ user: name });
        }, 360);

        return () => clearTimeout(timer);
    }, [name, updateStorage]);

    const settingsContainer = useRef<HTMLDivElement>(null);
    const [ready, setReady] = useState(false);
    useEffect(() => {
        if (!settingsContainer.current) return;

        const listener = () => {
            setReady(true);
        };
        setReady(false);
        if (edit) {
            settingsContainer.current.addEventListener(
                'transitionend',
                listener
            );
        } else {
            settingsContainer.current.addEventListener(
                'transitionend',
                listener
            );
        }

        return () =>
            settingsContainer.current &&
            settingsContainer.current.removeEventListener(
                'transitionend',
                listener
            );
    }, [settingsContainer, edit]);

    return (
        <div
            className={clsx('user', {
                isOpen: edit,
            })}
        >
            <div className={'user__widget'}>
                <button
                    className={clsx('button--icon', 'user__button', {
                        isActive: edit,
                    })}
                    onClick={() => setEdit(!edit)}
                >
                    <FaceIcon />
                </button>
                {edit ? (
                    <input
                        className="user__input"
                        type="text"
                        value={name}
                        onChange={({ target: { value } }) => setName(value)}
                    />
                ) : (
                    <span className="user__label">{user}</span>
                )}
            </div>
            <div
                ref={settingsContainer}
                className={clsx('user__settings', {
                    isReady: ready,
                    isOpen: edit,
                })}
            >
                <button
                    onClick={() =>
                        updateStorage({
                            scores: {},
                        })
                    }
                >
                    reset local highscores
                </button>
            </div>
        </div>
    );
}
