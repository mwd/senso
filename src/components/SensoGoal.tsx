import clsx from 'clsx';
import * as React from 'react';
import useSensoContext from '../hooks/useSensoContext';
import { calculateScore } from '../utils';
import './SensoGoal.scss';

export default function SensoGoal() {
    const { goal } = useSensoContext();

    return (
        <div className={clsx('sensoGoal', goal.type)}>
            <span className="sensoGoal__type">{goal.type}</span>
            {goal.type === 'win' ? (
                <div className={'sensoGoal__value'}>
                    {calculateScore(goal.score)}
                </div>
            ) : null}
            <span className="sensoGoal__message">{goal.message}</span>
        </div>
    );
}
