import clsx from 'clsx';
import * as React from 'react';
import { useCallback, useEffect, useRef, useState } from 'react';
import * as ReactDOM from 'react-dom';
import { isTouchDevice } from '../app';
import soundMap from '../soundMap';
import { Sound, Step } from '../types';
import './SensoPad.scss';
import { SensoSvgPad } from './SensoSvg';

interface SensoPadProps {
    sound?: Sound;
    playHead?: Step;
    onTap?: (sound: Sound) => void;
}

export default function SensoPad({ sound, playHead, onTap }: SensoPadProps) {
    const audioElement = useRef<HTMLAudioElement>(null);

    const play = useCallback(() => {
        if (!audioElement.current) return;
        audioElement.current.pause();
        audioElement.current.currentTime = 0;
        audioElement.current.play();

        if (onTap) onTap(sound);
    }, [audioElement.current, sound, onTap]);

    const [isPlaying, setIsPlaying] = useState(false);

    useEffect(() => {
        if (playHead && playHead.sound === sound) {
            play();
        }
    }, [audioElement, playHead, sound]);

    return (
        <>
            <SensoSvgPad
                className={clsx('sensoPad', {
                    isPlaying:
                        audioElement.current && !audioElement.current.paused,
                })}
                sound={sound}
                onMouseDown={
                    !isTouchDevice
                        ? () => {
                              play();
                          }
                        : undefined
                }
                onTouchStart={
                    isTouchDevice
                        ? (event) => {
                              event.stopPropagation();
                              play();
                          }
                        : undefined
                }
                onTouchEnd={
                    isTouchDevice
                        ? (event) => {
                              event.preventDefault();
                              event.stopPropagation();
                          }
                        : undefined
                }
            />
            {ReactDOM.createPortal(
                <audio
                    ref={audioElement}
                    src={soundMap(sound)}
                    onPlay={() => {
                        setIsPlaying(true);
                    }}
                    onPause={() => {
                        setIsPlaying(false);
                    }}
                />,
                document.getElementById('track')
            )}
        </>
    );
}
