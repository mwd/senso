import * as React from 'react';
import useCockpitContext from '../hooks/useCockpitContext';
import './SensoHighScores.scss';

const dateFormat = require('dateformat');

export default function SensoHighScores() {
    const { highScores } = useCockpitContext();

    return (
        <div className="highScores">
            <h3>highscores</h3>
            <ol className="highScores__list">
                {highScores.length ? (
                    highScores.map((highScore) => (
                        <li
                            className="highScores__item"
                            key={highScore.date + highScore.user}
                        >
                            <span className="highScores__score">
                                {
                                    highScore.score
                                        .replace(/(0+)(\d+)/, '$1#$2')
                                        .split('#')[1]
                                }
                            </span>
                            <span className="highScores__meta">
                                <span className="highScores__meta__user">
                                    {highScore.user}
                                </span>
                                <span className="highScores__meta__date">
                                    {dateFormat(
                                        new Date(highScore.date * 1000),
                                        'dd.mm.yyyy / HH:MM'
                                    )}
                                </span>
                            </span>
                        </li>
                    ))
                ) : (
                    <p>no highscores yet. be the first!</p>
                )}
            </ol>
        </div>
    );
}
