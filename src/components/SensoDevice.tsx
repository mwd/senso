import clsx from 'clsx';
import * as React from 'react';
import { useEffect, useRef, useState } from 'react';
import * as ReactDOM from 'react-dom';
import useSensoContext from '../hooks/useSensoContext';
import soundMap from '../soundMap';
import { Difficulty, Goal, Recording, Score, Sound, Track } from '../types';
import LabeledButton from './LabeledButton';
import './SensoDevice.scss';
import SensoPad from './SensoPad';
import SensoSvg from './SensoSvg';

const generateScoreObject = (
    recording: Recording,
    track: Track,
    difficulty: Difficulty
): Score => ({
    recording,
    time: recording.stop - recording.start,
    totalTime: track.steps.reduce((time: number, step) => {
        return (time += step.duration);
    }, 0),
    steps: track.steps.length,
    rating: track.rating,
    difficulty,
    synced: false,
});

interface SensoDeviceProps {
    onPlay?: (message?: string) => void;
    onCapturePrepare?: (message?: string) => void;
    onCapture?: (message?: string) => void;
    onGoal?: (goal: Goal) => void;
    onStop?: (message?: string) => void;
}

export default function SensoDevice({
    onCapturePrepare,
    onCapture,
    onPlay,
    onStop,
    onGoal,
}: SensoDeviceProps) {
    //
    const {
        difficulty,
        currentTrack: track,
        deviceState,
        setDeviceState,
    } = useSensoContext();

    const [playing, setPlaying] = useState(false);
    const [playHead, setPlayHead] = useState(0);

    const [prepare, setPrepare] = useState(false);
    const [recHead, setRecHead] = useState(0);
    const [response, setResponse] = useState<Sound | null>(null);
    const [recording, setRecording] = useState<Recording | null>(null);

    const audioWin = useRef<HTMLAudioElement>(null);
    const audioFail = useRef<HTMLAudioElement>(null);

    const win = () => {
        const nextRecording = endRecord();
        audioWin.current && audioWin.current.play();
        onGoal &&
            onGoal({
                type: 'win',
                score: generateScoreObject(nextRecording, track, difficulty),
            });
    };

    const fail = (message: string) => {
        endRecord();
        audioFail.current && audioFail.current.play();
        onGoal &&
            onGoal({
                type: 'fail',
                score: null,
                message,
            });
    };

    const stop = () => {
        setPlaying(false);
        setPlayHead(0);
        setPrepare(false);
        onStop && onStop();
        setDeviceState('idle');
    };

    const play = () => {
        if (prepare) setPrepare(false);
        onPlay && onPlay();
        setDeviceState('playing');
    };

    const prepareRecording = () => {
        setPlaying(false);
        setPlayHead(0);
        setRecording(null);
        onCapturePrepare && onCapturePrepare();
        setDeviceState('preparing');
    };

    const record = () => {
        if (recHead !== 0) return;

        setRecording({
            start: new Date().getTime(),
            stop: null,
        });
        onCapture && onCapture();
        setDeviceState('recording');
    };

    const endRecord = (): Recording => {
        const nextRecording = {
            ...recording,
            stop: new Date().getTime(),
        };
        setRecording(nextRecording);
        setRecHead(0);
        setResponse(null);
        setPrepare(false);
        onStop && onStop();
        setDeviceState('idle');
        return nextRecording;
    };

    // prepare capturing
    useEffect(() => {
        if (prepare) prepareRecording();
    }, [prepare, setRecHead, setResponse, setRecording, onCapturePrepare]);
    // capture
    useEffect(() => {
        if (!prepare) return undefined;

        // wait for first response to start capturing
        if (!response && recHead === 0) {
            return undefined;
        }

        record();
        let timer;

        if (response) {
            if (response === track.steps[recHead].sound) {
                if (recHead === track.steps.length - 1) {
                    win();
                } else {
                    setRecHead(recHead + 1);
                    setResponse(null);
                }
            } else {
                fail('wrong sound');
            }
            clearTimeout(timer);
        } else {
            timer = setTimeout(() => {
                fail('too slow');
            }, (track.steps[recHead].duration / difficulty) * 3);
        }
        return () => clearTimeout(timer);
    }, [prepare, recHead, response]);

    // play
    useEffect(() => {
        if (!playing) return undefined;

        play();
        let timer;

        if (playHead < track.steps.length - 1) {
            timer = setTimeout(() => {
                setPlayHead(playHead + 1);
            }, track.steps[playHead].duration / difficulty);
        } else {
            stop();
        }
        return () => clearTimeout(timer);
    }, [playing, playHead, setPlayHead, track]);

    return (
        <>
            <div className="sensoDevice">
                <SensoSvg>
                    {['green', 'red', 'blue', 'yellow'].map((sound: Sound) => (
                        <SensoPad
                            key={sound}
                            sound={sound}
                            playHead={playing && track.steps[playHead]}
                            onTap={
                                prepare
                                    ? (sound) => {
                                          setResponse(sound);
                                      }
                                    : undefined
                            }
                        />
                    ))}
                </SensoSvg>

                <div className="sensoDevice__controls">
                    <LabeledButton
                        buttonClassName="sensoDevice__button"
                        variant="listen"
                        label={playing ? 'stop' : 'listen'}
                        isActive={playing}
                        onClick={() => (playing ? stop() : setPlaying(true))}
                    />
                    <LabeledButton
                        buttonClassName={clsx(
                            'sensoDevice__button',
                            deviceState
                        )}
                        variant="play"
                        label={prepare ? 'stop' : 'play'}
                        isActive={prepare}
                        onClick={() => (prepare ? stop() : setPrepare(true))}
                        isWaiting={deviceState === 'preparing'}
                    />
                </div>
            </div>

            {ReactDOM.createPortal(
                <>
                    <audio ref={audioWin} src={soundMap('win')} />
                    <audio ref={audioFail} src={soundMap('fail')} />
                </>,
                document.getElementById('track')
            )}
        </>
    );
}
