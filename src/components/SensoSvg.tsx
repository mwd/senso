import clsx from 'clsx';
import * as React from 'react';
import { ReactNodeArray } from 'react';
import { Sound } from '../types';
import './SensoSvg.scss';

const svgPathMap = (sound: Sound) => {
    switch (sound) {
        case 'blue':
            return 'M321,419.2c50-8.8,89.4-48.2,98.2-98.2h159.1c-2.4,33-10.6,64.9-24.2,94.7 c-13.8,30.3-32.9,57.7-56.7,81.5s-51.3,42.9-81.5,56.7c-29.9,13.6-61.7,21.8-94.7,24.2V419.2H321z';
        case 'green':
            return 'M21.8,279c2.4-33,10.6-64.9,24.2-94.7c13.8-30.3,32.9-57.7,56.7-81.5c23.9-23.9,51.3-43,81.6-56.8 C214.1,32.4,246,24.2,279,21.8v159.1c-50,8.8-89.4,48.2-98.2,98.2h-159V279z';
        case 'red':
            return 'M419.2,279c-8.8-50-48.2-89.4-98.2-98.2v-159c33,2.4,64.9,10.6,94.7,24.2 c30.3,13.8,57.7,32.9,81.5,56.7s42.9,51.3,56.7,81.5c13.6,29.9,21.8,61.7,24.2,94.7H419.2V279z';
        case 'yellow':
            return 'M279,578.2c-33-2.4-64.9-10.6-94.7-24.2c-30.3-13.8-57.7-32.9-81.5-56.7 c-23.9-23.9-43-51.3-56.8-81.6C32.4,385.9,24.2,354,21.8,321h159.1c8.8,50,48.2,89.4,98.2,98.2v159H279z';
        default:
            return null;
    }
};

export const SensoSvgBase = () => (
    <path
        className="sensoSvg__base"
        d="M300,595c-78.8,0-152.9-30.7-208.6-86.4C35.7,452.9,5,378.8,5,300S35.7,147.1,91.4,91.4
	C147.1,35.7,221.2,5,300,5s152.9,30.7,208.6,86.4C564.4,147.1,595,221.2,595,300s-30.7,152.9-86.4,208.6
	C452.9,564.3,378.8,595,300,595z"
    />
);

interface SensoSvgPadProps {
    sound: Sound;
    withoutSoundClass?: boolean;
    [index: string]: any;
}
export const SensoSvgPad = ({
    sound,
    withoutSoundClass,
    className,
    ...props
}: SensoSvgPadProps) =>
    React.createElement('path', {
        ...props,
        className: clsx(
            'sensoSvg__pad',
            withoutSoundClass ? null : sound,
            className
        ),
        d: svgPathMap(sound),
    });

interface SensoSvgProps {
    children?: React.ReactNode | ReactNodeArray;
    withoutSoundClass?: SensoSvgPadProps['withoutSoundClass'];
    [index: string]: any;
}

export default function SensoSvg({
    children,
    className,
    withoutSoundClass,
    ...props
}: SensoSvgProps) {
    return (
        <svg
            className={clsx('sensoSvg', className)}
            version="1.1"
            x="0px"
            y="0px"
            viewBox="0 0 600 600"
            xmlSpace="preserve"
            {...props}
        >
            <SensoSvgBase />
            {children ||
                ['green', 'red', 'blue', 'yellow'].map((sound: Sound) => (
                    <SensoSvgPad
                        sound={sound}
                        withoutSoundClass={withoutSoundClass}
                    />
                ))}
        </svg>
    );
}
