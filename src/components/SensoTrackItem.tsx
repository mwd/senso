import * as React from 'react';
import useStoreContext from '../hooks/useStoreContext';
import { Track } from '../types';
import Rating from './common/Rating';
import './SensoTrackItem.scss';
import SensoTracksScore from './SensoTracksScore';

interface SensoTrackItemProps {
    track: Track;
}

export default function SensoTrackItem({ track }: SensoTrackItemProps) {
    const { scores } = useStoreContext();

    return (
        <div className="sensoTrackItem">
            <span className="sensoTrackItem__label">
                {track.name || track.id}
            </span>
            <Rating
                className="sensoTrackItem__rating"
                rating={5}
                ratingValue={track.rating}
            />
            {scores[track.id] ? (
                <SensoTracksScore
                    key={JSON.stringify(scores[track.id])}
                    trackId={track.id}
                    {...scores[track.id]}
                />
            ) : (
                <div className="sensoTrackItem__score">not scored</div>
            )}
        </div>
    );
}
