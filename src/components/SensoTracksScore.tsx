import * as React from 'react';
import { useState } from 'react';
import useCockpitContext from '../hooks/useCockpitContext';
import { Score, Track } from '../types';
import { calculateScore } from '../utils';
import CloudUploadIcon from './icons/CloudUploadIcon';

interface SensoTracksScore extends Score {
    trackId: Track['id'];
}

export default function SensoTracksScore({
    trackId,
    ...score
}: SensoTracksScore) {
    const { uploadScore } = useCockpitContext();

    const [synced, setSynced] = useState(score.synced);

    return (
        <div className="sensoTrackItem__score">
            <div>{`your score: ${calculateScore(score)}`}</div>
            {!synced ? (
                <button
                    className={'sensoTrackItem__upload-score'}
                    onClick={() => {
                        setSynced(true);
                        uploadScore(score, trackId);
                    }}
                >
                    <CloudUploadIcon />
                </button>
            ) : null}
        </div>
    );
}
