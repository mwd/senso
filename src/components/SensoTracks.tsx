import clsx from 'clsx';
import * as React from 'react';
import useCockpitContext from '../hooks/useCockpitContext';
import useSensoContext from '../hooks/useSensoContext';
import useStoreContext from '../hooks/useStoreContext';
import SensoTrackItem from './SensoTrackItem';
import './SensoTracks.scss';

export default function SensoTracks() {
    const { scores, currentTrackId } = useStoreContext();
    const { selectTrack } = useSensoContext();

    // todo check if context makes more sense for passing all the data down
    const { tracks } = useCockpitContext();

    return (
        <div className={'sensoTracks'}>
            <h3>tracks</h3>
            <ul className="sensoTracks__list">
                {tracks.length &&
                    tracks.map((track) => (
                        <li
                            key={track.id + scores[track.id]}
                            className={clsx('sensoTracks__item', {
                                isSelected: currentTrackId === track.id,
                            })}
                            onClick={() => selectTrack(track.id)}
                        >
                            <SensoTrackItem track={track} />
                        </li>
                    ))}
            </ul>
        </div>
    );
}
