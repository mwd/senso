import clsx from 'clsx';
import * as React from 'react';
import './LabeledButton.scss';

interface LabeledButtonProps {
    label: string;
    className?: string;
    buttonClassName?: string;
    variant?: 'listen' | 'play';
    isActive?: boolean;
    isWaiting?: boolean;
    [index: string]: any;
}

export default function LabeledButton({
    label,
    className,
    buttonClassName,
    variant,
    isActive,
    isWaiting,
    ...props
}: LabeledButtonProps) {
    return (
        <div className={clsx(className, 'labeledButton')} {...props}>
            <button
                className={clsx(
                    'labeledButton__button',
                    buttonClassName,
                    variant,
                    {
                        isActive,
                        isWaiting,
                    }
                )}
            />
            <span className="labeledButton__label">{label}</span>
        </div>
    );
}
