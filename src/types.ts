export type Collections = 'sensoHighScores' | 'sensoTracks';

// senso
import { sounds } from './soundMap';
export type Sound = keyof typeof sounds;

export interface Step {
    sound: Sound;
    duration: number;
}

export interface Track {
    id: string;
    name?: string;
    steps: Step[];
    rating: number;
}

export interface Recording {
    start: number;
    stop: number;
}

export interface Score {
    recording: Recording;
    time: number;
    totalTime: number;
    steps: number;
    rating: number;
    difficulty: number;
    synced: boolean;
}

export type Goal = {
    type: null | 'win' | 'fail';
    score: Score | null;
    message?: string;
};

export type DeviceState =
    | 'idle'
    | 'preparing'
    | 'recording'
    | 'playing'
    | 'buffer';

export type Difficulty = 1 | 2 | 3;

export interface HighScore {
    date: number;
    user?: string;
    score: string;
}

export interface CockpitContext {
    tracks: Track[];
    highScores: HighScore[];
    uploadScore: (score: Score, trackId: Track['id']) => void;
    synced: number;
    error: any;
}

export const defaultCockpitContext: CockpitContext = {
    tracks: [],
    highScores: [],
    uploadScore: (s, ti) => null, // ti > track ID ?
    synced: 0,
    error: null,
};

export interface SensoStore {
    user: string;
    difficulty: Difficulty;
    scores: {
        [index: string]: Score;
    };
    currentTrackId: Track['id'] | null;
}

export interface SensoStoreContext extends SensoStore {
    updateStorage: (update: Partial<Storage>) => void;
    synced: number;
}

export const defaultStore = {
    difficulty: 2 as Difficulty,
    user: 'New User',
    scores: {},
    currentTrackId: null,
};

export const defaultStoreContext = {
    ...defaultStore,
    updateStorage: (u) => null,
    synced: 0,
};

// todo consider using a reducer-like updated function instead of dedicated setter
export interface SensoContext {
    currentTrack: null | Track;
    selectTrack: (key: Track['id']) => void;
    deviceState: DeviceState;
    setDeviceState: (gameState: DeviceState) => void;
    goal: Goal;
    handleGoal: (goal: Goal) => void;
    // score: Score | null;
    // setScore: (score: Score) => void;
    difficulty: SensoStore['difficulty'];
    setDifficulty: (difficulty: SensoStore['difficulty']) => void;
}

export const defaultSensoContext: SensoContext = {
    currentTrack: null,
    selectTrack: (k: Track['id']) => null,
    deviceState: 'idle',
    setDeviceState: (g: SensoContext['deviceState']) => null,
    goal: { type: null, score: null },
    handleGoal: (g) => null,
    // score: null,
    // setScore: (s) => null,
    difficulty: 2,
    setDifficulty: (d: SensoContext['difficulty']) => null,
};
