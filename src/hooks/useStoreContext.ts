import { useContext } from 'react';
import { context } from '../provider/StoreProvider';
import { SensoStoreContext } from '../types';

export default function useStoreContext() {
    return useContext<SensoStoreContext>(context);
}
