import { useContext } from 'react';
import { context } from '../provider/CockpitProvider';
import { CockpitContext } from '../types';

export default function useCockpitContext() {
    return useContext<CockpitContext>(context);
}
