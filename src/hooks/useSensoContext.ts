import { useContext } from 'react';
import { context } from '../provider/SensoProvider';
import { SensoContext } from '../types';

export default function useSensoContext() {
    return useContext<SensoContext>(context);
}
