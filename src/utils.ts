import { Score } from './types';

export function padDigits(number, digits) {
    return (
        Array(Math.max(digits - String(number).length + 1, 0)).join('0') +
        number
    );
}

// todo reconsider location for game logic
export function calculateScore(score: Score | null) {
    if (!score) return '0';

    const timeAspect = score.totalTime / score.time;
    const stepAspect = (score.totalTime * score.steps) / 100;
    const baseScore = Math.floor(timeAspect * stepAspect);
    return Math.floor(baseScore * score.rating * score.difficulty * 4);
}
