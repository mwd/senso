const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractWebpackPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const HTMLPlugin = new HtmlWebpackPlugin({
    title: 'mwd portfolio',
    template: './src/index.html',
    inject: true,
    minify: {
        removeComments: true,
        collapseWhitespace: false,
    },
});

const MiniCssExtractPlugin = new MiniCssExtractWebpackPlugin({
    filename: '[name].css',
    chunkFilename: '[id].css',
});

const CopyPlugin = new CopyWebpackPlugin([
    {
        from: './src/assets/**/*.mp3',
        to: 'assets/audio',
        flatten: true,
    },
    {
        from: './src/assets/favicon/*.*',
        to: 'favicon',
        flatten: true,
    },
]);

module.exports = {
    entry: ['./src/app.tsx'],
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname, 'dist'),
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js', '.scss'],
        alias: {
            cssReset: path.join(
                __dirname,
                'node_modules/modern-css-reset/dist/reset.min.css'
            ),
        },
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.(s*)css$/,
                use: [
                    MiniCssExtractWebpackPlugin.loader,
                    'css-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.(png|jpe?g|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'assets/',
                            publicPath: './assets/',
                        },
                    },
                ],
            },
            {
                test: /\.svg$/,
                use: [
                    {
                        loader: 'svg-inline-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'assets/',
                            publicPath: './assets/',
                        },
                    },
                ],
            },
            {
                test: /.(ttf|otf|eot|woff(2)?)(\?[a-z0-9]+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'assets/fonts/',
                            publicPath: './assets/fonts/',
                        },
                    },
                ],
            },
            {
                test: /\.html$/i,
                use: [
                    {
                        loader: 'html-loader',
                    },
                ],
            },
        ],
    },
    devtool: 'source-map',
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000,
        hot: false,
        liveReload: true,
    },
    plugins: [HTMLPlugin, MiniCssExtractPlugin, CopyPlugin],
};
